import react from 'react';
import TodoListItem from './todo-list-item';

const TodoList = () => {
  return (
    <ul>
      <li><TodoListItem todo='eat breakfast' /></li>
      <li><TodoListItem todo='drink cola' /></li>
      <li><TodoListItem todo='see sky' /></li>
      <li><TodoListItem todo='eat burger' /></li>
    </ul>
  )
}

export default TodoList;
